package org.academiadecodigo.wordsgame.entities.users;

public enum Roles {

    ADMIN,
    PLAYER
}
